<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LocationSent implements  ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    /**
     * @var $location
     */

    public $lat;
    public $long;


    public function __construct($lat,$long)
    {

        $this->lat=$lat;
        $this->long=$long;
    }

    public function broadcastWith()
    {
        return [
            'location' => [
                "lat" => $this->lat,
                "long" => $this->long
            ]
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        //Nombre del canal
        return new Channel('location');
    }
}
