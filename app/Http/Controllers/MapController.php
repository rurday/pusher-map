<?php

namespace App\Http\Controllers;



use App\Events\LocationSent;
use App\Map;
use Illuminate\Http\Request;

class MapController extends Controller
{

    public function fetchLocation(){
        $location = Map::orderBy("created_at", "DESC")->get();


        return response()->json($location);
    }


    public function sendLocation(Request $request){


        $lat= $request->input('lat', 100001);
        $long= $request->input('long', 100001);

        $location = ["lat"=>$lat , "long"=>$long];

        $obj = Map::create($location);

        $lati = $obj->lat;
        $longi= $obj->long;

        try {
                broadcast(new LocationSent($lati,$longi));
        } catch (\Exception $exception) {
            \Log::error($exception);
            throw $exception;
        }

        return response()->json(['status' => 'Location Sent!']);

    }
}
