
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('chat-messages', require('./components/ChatMessages.vue'));
Vue.component('chat-form', require('./components/ChatForm.vue'));
Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app',
    data: {
        locations: []
    },

    created() {
        this.fetchLocations();

        Echo.channel('location')
            .listen('LocationSent', (e) => {
                this.locations.unshift({
                    lat: e.location.lat,
                    long: e.location.long
                });
            });
    },

    methods: {
        fetchLocations() {
            axios.get('/api/map').then(response => {

                this.locations = response.data;
            });
        },
        addLocation(location) {

            arreglo= location.location.split(",");

            var obj = new Object();
            obj.lat = arreglo[0];
            obj.long = arreglo[1];

            axios.post('/api/map', obj).then(response => {});
        }
    }
});