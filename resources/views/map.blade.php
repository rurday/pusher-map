@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Map</div>

                    <div class="panel-body">
                        {{--Muestra nuestros mensajes--}}
                        <chat-messages :locations="locations" ></chat-messages>
                    </div>
                    <div class="panel-footer">
                        {{-- Input field y boton para enviar mensajes--}}
                        <chat-form
                                v-on:locationsent="addLocation"

                        ></chat-form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection